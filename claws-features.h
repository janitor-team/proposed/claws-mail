/* claws-features.h.  Generated from claws-features.h.in by configure.  */
#define HAVE_DBUS_GLIB 1
#define HAVE_DIRENT_D_TYPE 1
#define HAVE_GPGME_PKA_TRUST 1
#define HAVE_LIBCOMPFACE 1
#define HAVE_LIBETPAN 1
#define HAVE_LIBSM 1
/* #undef HAVE_NETWORKMANAGER_SUPPORT */
#define HAVE_STARTUP_NOTIFICATION 1
#define HAVE_VALGRIND 1
#define HAVE_SVG 1
/* #undef USE_BOGOFILTER_PLUGIN */
#define USE_ENCHANT 1
#define USE_GNUTLS 1
#define USE_GPGME 1
#define USE_JPILOT 1
#define USE_LDAP 1
#define USE_LDAP_TLS 1
/* #undef USE_ALT_ADDRBOOK */
#define USE_PTHREAD 1
/* #undef USE_SPAMASSASSIN_PLUGIN */
/* #undef __CYGWIN__ */
